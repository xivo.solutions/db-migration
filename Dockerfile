FROM tianon/postgres-upgrade:11-to-15

# Add fr_FR.UTF-8 to handle upgrade of database in this locale
RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
